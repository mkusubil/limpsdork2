<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Exercice 4</title>
  </head>
  <body>
    <header>
      <div class="container-fluid justify-content-center p-0">
        <div class="row align-items-center px-0 justify-content-md-center color1 ">
          <div class="col-2 pl-md-0 ml-md-n4 ">
            <img src="https://flink.apache.org/img/logo/png/500/flink_squirrel_500.png" alt="logo" id="logo">
          </div>
          <div class="col-auto ml-5 ml-md-0  text-center">
            <h1>Форма</h1>
          </div>
        </div>
      </div>
      </header>
      <div class="table & form">
          <div class="col-sm-12 col-md-6 mx-sm-0  mx-md-auto color3 order-md-0 mt-2  ">
            <h2>Форма</h2>
            <form action="index.php" method="POST">
              <label>
              Имя
              <br>
              <input name="name" value="" placeholder="Name">
              </label>
              <br>
              <label>
              Email
              <br>
              <input name="email" value="" type="email" placeholder="yourmail@gmail.com">
              </label>
              <br>
              <label>
              Дата рождения
              <br>
              <input name="birthday" value="2000-01-01" type="date" min="1940-01-01" max="2017-01-01">
              </label>
              <br>
              <p>
                Пол
              </p>
              <br>
              <label>
              <input type="radio" checked="checked" name="sex" value="MAN"> М
              </label>
              <label>
              <input type="radio" name="sex" value="WOM"> Ж
              </label>
              <br>
              <p>
                Количество конечностей
              </p>
              <br>
              <label>
              <input type="radio" checked="checked" name="amlimb" value="4"> 4
              </label>
              <label>
              <input type="radio" name="amlimb" value="3"> 3
              </label>
              <label>
              <input type="radio" name="amlimb" value="2"> 2
              </label>
              <label>
              <input type="radio" name="amlimb" value="1"> 1
              </label>
              <label>
              <input type="radio" name="amlimb" value="0"> 0
              </label>
              <br>
              <label>
                Суперспособности
                <br>
                <select name="superpower[]" multiple="multiple" class="custom-select">
                  <option value="god">бессмертие</option>
                  <option value="twalk">прохождение сквозь стены</option>
                  <option value="fly">левитация</option>
                </select>
              </label>
              <br>
              <label>
              Биография
              <br>
              <textarea name="biography" placeholder="Your biography"></textarea>
              </label>
              <br>
              <label>
              <input type="checkbox" checked="checked" name="yessayer" value="yes"> С контрактом ознакомлен
              </label>
              <br>
              <button class="btn btn-success">Отправить</button>
            </form>
          </div>
          <div class="col-0 col-md-1">
          </div>
        </div>
    <footer>
      <div class="row color3 mt-2">
        <div class="col-12 ">
          <h2>
            (c)Матвей Скирда
          </h2>
        </div>
      </div>
    </footer>
  </body>
</html>
