<?php
/**
 * Интернет-программирование. Задача 8.
 * Реализовать скрипт на веб-сервере на PHP или другом языке,
 * сохраняющий в XML-файл заполненную форму задания 7. При
 * отправке формы на сервере создается новый файл с уникальным именем.
 */

$ability_labels = ['god' => 'бессмертие', 'fly' => 'левитация', 'twalk' => 'прохождение сквозь стены'];
$sex_data = ['MAN','WOM'];

header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;
if (empty( $_POST['name'])) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}
else if(strlen( $_POST['name']) > 20) {
  print('Недопустимая длина имени<br/>');
  $errors = TRUE;
}
else if (!preg_match('/^[а-яА-Я ]+$/u', $_POST['name'])) {
    print('Недопустимые символы в имени.<br/>');
    $errors = TRUE;
}

if (empty($_POST['email'])) {
  print ('Заполните email.<br/>');
  $errors = TRUE;
}
else if(strlen($_POST['email']) > 30) {
  print ('Недопустимая длина email<br/>');
  $errors = TRUE;
}
else if (!preg_match('/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u', $_POST['email'])) 
{
    print('Недопустимые символы в email.<br/>');
    $errors = TRUE;
}

if (empty($_POST['sex'])) {
    print ('Выберете пол .<br/>');
    $errors = TRUE;
}
else if(!in_array($_POST['sex'], $sex_data)){
    print ('Неизвестный пол .<br/>');
    $errors = TRUE;
}

if(empty($_POST['amlimb'])) {
    print ('Выбирите количество конечностоей.<br/>');
    $errors = TRUE;
}else if($_POST['amlimb'] > 4 || $_POST['amlimb'] < 0){
    print ('Такого числа нет.<br/>');
    $errors = TRUE;
}


if(empty($_POST['biography'])) {
   print ('Заполните биографию.<br/>');
   $errors = TRUE;
 }
else if (strlen($_POST['biography']) > 500) {
  print ('Недопустимая длина биографии<br/>');
  $errors = TRUE;
}

 if(empty($_POST['yessayer'])) {
     $_POST['yessayer'] = 'NO';
 }else {
     $_POST['yessayer'] = 'YES';
 }
 
 $ability_data = array_keys($ability_labels);
 if (empty($_POST['superpower'])) {
     print('Выберите способность.<br/>');
     $errors = TRUE;
 }
 else{
     $abilities = $_POST['superpower'];
     foreach ($abilities as $ability) {
         if (!in_array($ability, $ability_data)) {
             print('Плохая способность!<br/>');
             $errors = TRUE;
         }
     }
 }
 
 
 
 
 

// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
// *************

if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  exit();
}

$_POST['superpower'] = implode(' ',$_POST['superpower']);

// Сохранение в базу данных.

$user = 'u20362';
$pass = '5800777';
$db = new PDO('mysql:host=localhost;dbname=u20362', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("INSERT INTO Visitor SET name = ?,email = ?,birthday = ?,sex = ?,amlimb = ?,superpower = ?,biography = ?,yessayer = ?");
  $stmt -> execute(array($_POST['name'],$_POST['email'],$_POST['birthday'],$_POST['sex'],$_POST['amlimb'],$_POST['superpower'],$_POST['biography'],$_POST['yessayer']));
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));
 
//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1');
